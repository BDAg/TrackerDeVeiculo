import { Http } from '@angular/http';
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';

@Injectable()
export class BusServiceProvider {
  data: any;
  constructor(public http: Http) {
    
  }

  callTracker(idTracker) {
    
    return new Promise(resolve => {
      this.http
        .get(
          `http://populus-alternative.herokuapp.com/tracker/${idTracker}/?format=json`
        )
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  callPoint() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http
        .get(
          `http://populus-alternative.herokuapp.com/pontos/?format=json`
        )
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  callLinhaPonto() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http
        .get(
          `http://populus-alternative.herokuapp.com/linhaPonto/?format=json`
        )
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  callLinha(idLinha) {

    return new Promise(resolve => {
      this.http
        .get(
          `http://populus-alternative.herokuapp.com/linhas/${idLinha}/?format=json`
        )
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }
}
