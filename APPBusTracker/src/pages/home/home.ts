import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { BusServiceProvider } from '../../providers/bus-service/bus-service';


declare var google;

@Component({
  selector: 'home-page',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  origem = this.mapElement;
  destino = this.mapElement;
  map: any;


  pt: any = [];
  ponto: any = [];
  linhaPonto: any = [];

  idTracker: any;

  constructor(public navCtrl: NavController, public geolocation: Geolocation, public busService: BusServiceProvider) {
    this.getPonto();
    this.getLinhaPonto();

  }

  getPonto() {
    this.busService.callPoint()
      .then(data => {
        for (let i = 0; i < data.length; i++) {
          this.pt.push([parseFloat(data[i].lat), parseFloat(data[i].lon)]);
          this.ponto[i] = new google.maps.LatLng(this.pt[i][0], this.pt[i][1]);
        }
      });
  }

  getLinhaPonto() {
    this.busService.callLinhaPonto()
      .then(data => {
        for (let i = 0; i < data.length; i++) {
          this.linhaPonto.push([data[i].hora, data[i].nomeLinha, data[i].ponto])
        }
      })
  }

  ionViewDidLoad() {

    let posicaoPassageiro = new google.maps.Marker({
      map: this.map,
      position: null,
      icon: 'https://i.stack.imgur.com/VpVF8.png'
    });

    var infoWindow = new google.maps.InfoWindow({
      content: "<h4>Você está aqui</h4>"
    });

    google.maps.event.addListener(posicaoPassageiro, 'click', function () {
      infoWindow.open(this.map, posicaoPassageiro);
    });

    this.geolocation.getCurrentPosition().then((position) => {
      let localizacaoAtual = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      this.initMap(localizacaoAtual)
    });

    let options = {
      enableHighAccuracy: true
    }

    this.geolocation.watchPosition(options).subscribe((position) => {
      let localizacaoAtual = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      posicaoPassageiro.setMap(null)
      posicaoPassageiro.setPosition(localizacaoAtual);
      posicaoPassageiro.setMap(this.map)
    }, (err) => {
      console.log(err);
    });
  }

  initMap(localizacaoAtual) {

    this.geolocation.getCurrentPosition().then((position) => {

      let mapOptions = {
        center: localizacaoAtual,
        streetViewControl: false,
        zoom: 16,
        mapTypeControlOptions: {
          mapTypeIds: [
            google.maps.MapTypeId.ROADMAP,
            google.maps.MapTypeId.SATELLITE
          ]
        }
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      let origem = this.origem;
      let destino = this.destino;
      let iOrigem = null;
      let iDestino = null;

      let p: any = [];

      for (let i = 0; i < this.pt.length; i++) {
        p[i] = new google.maps.Marker({
          position: {
            lat: this.pt[i][0],
            lng: this.pt[i][1],
          },
          map: this.map
        });

        google.maps.event.addListener(p[i], 'click', (event) => {
          let posicao = p[i].getPosition()
          p[i].setAnimation(google.maps.Animation.BOUNCE);
          if (origem == null) {
            iOrigem = i + 1;
            return origem = posicao
          } else if (destino == null) {
            iDestino = i + 1;
            let linha = this.confereLinhaPonto(iOrigem, iDestino);
            if (linha != false) {
              let rota = this.procuraPontosLinha(linha);
              this.fazRota(rota, origem, posicao);
              this.chamaOnibus(linha);
            }
            return destino = posicao
          } else
            // let directionsDisplay = new google.maps.DirectionsRenderer;
            // directionsDisplay.setDirections(null);
            if (iOrigem != i) {
              p[iOrigem].setAnimation(null);
            }
          if (iDestino != i) {
            p[iDestino].setAnimation(null);
          }
          destino = null;
          iDestino = null;
          iOrigem = i;
          return origem = posicao
        }
        );
      }
    }, (err) => {
      console.log(err);
    });
  }


  confereLinhaPonto(iOrigem, iDestino) {
    let linha = null;
    // olha origem - destino e vê se estão na mesma linha (API)
    for (let i = 0; i < this.linhaPonto.length; i++) {
      if (iOrigem == this.linhaPonto[i][2] && linha == null) {
        linha = this.linhaPonto[i][1];
      }
      else if (iDestino == this.linhaPonto[i][2] && linha == null) {
        linha = this.linhaPonto[i][1];
      }
      else if (iOrigem == this.linhaPonto[i][2]) {
        if (linha == this.linhaPonto[i][1]) {
          return linha;
        }
      }
      else if (iDestino == this.linhaPonto[i][2]) {
        if (linha == this.linhaPonto[i][1]) {
          return linha;
        }
      }
    }
    if (linha == null) {
      return false;
    }

  }

  procuraPontosLinha(linha) {
    let rota = [];
    for (let i = 0; i < this.linhaPonto.length; i++) {
      if (linha == this.linhaPonto[i][1]) {
        rota.push({
          location: this.ponto[(this.linhaPonto[i][2] - 1)],
          stopover: true
        });
      }
    }
    return rota
  }


  fazRota(rota, origem, destino) {
    let directionsService = new google.maps.DirectionsService;
    let directionsRenderer = new google.maps.DirectionsRenderer;
    directionsRenderer.setMap(this.map);

    directionsService.route({
      origin: origem,
      destination: destino,
      waypoints: rota,
      travelMode: google.maps.TravelMode['DRIVING']
    }, (res, status) => {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsRenderer.setDirections(res);
      } else {
        console.warn(status);
      }
    });
  }
  chamaOnibus(linha) {
    console.log(linha)
    this.busService.callLinha(linha).then((data:any) => {
    this.idTracker = data.idTracker;
    console.log("trackerId: ", this.idTracker);
    this.busService.callTracker(this.idTracker)
      .then((data:any) => {
        // posTracker.push(data.lat, data.lon)
        console.log('Data: ', data.lat)
        console.log('Data: ', data.lon)

        let posOnibus = new google.maps.Marker({
          map: this.map,
          position: {
            lat: data.lat,
            lng: data.lon,
          },
          icon: 'https://i.imgur.com/meMeD4H.png'
        });
      })
  })
  }
}
