# Welcome to Vehicle Tracker

This project is directed to development of the vehicle tracking and your applications, mainly on bus.


## [Topics](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Home)

 - [Team](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team)
    - [Álvaro L. Cavalcante](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#%C3%A1lvaro-l-cavalcante)
    - [Flávio H. Licorio](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#fl%C3%A1vio-h-licorio)
    - [Gabriel Lauletta](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#gabriel-lauletta)
    - [Juvenal Yoshikawa](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#juvenal-yoshikawa)
    - [Lucas Brito](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#lucas-brito)
    - [Luiz Fernando](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#luiz-fernando)
    - [Samuel Licorio](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#samuel-licorio)
    - [Vitor Vieira](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/Team#vitor-vieira)
 - [Environment](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/environment)
   - [BackEnd](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/environment#backend)
   - [FrontEnd](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/environment#frontend)
   - [API](https://gitlab.com/BDAg/TrackerDeVeiculo/wikis/environment#api)