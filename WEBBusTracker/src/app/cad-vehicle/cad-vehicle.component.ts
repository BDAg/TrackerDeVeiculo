import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-cad-vehicle',
  templateUrl: './cad-vehicle.component.html',
  styleUrls: ['./cad-vehicle.component.css']
})
export class CadVehicleComponent implements OnInit {
  formCadVehicle;
  formValues: Object;

  dataPosted: any;


  constructor(private formBuilder: FormBuilder, public httpClient: HttpClient, private router: Router) { }

  ngOnInit() {
    this.formCadVehicle = this.formBuilder.group({
      tipo: ['', Validators.required],
      categoria: ['', Validators.required],
      marca: ['', Validators.required],
      modeloVeiculo: ['', Validators.required],
      numeroPlaca: ['', Validators.required],
      numeroFrota: ['', Validators.required],
      status: ['', Validators.required],
      numeroRastreador: ['', Validators.required],
    });
    setTimeout(() => {
      this.formCadVehicle.patchValue({
        tipo: '',
        categoria: '',
        marca: '',
        modeloVeiculo: '',
        numeroPlaca: '',
        numeroFrota: '',
        status: '',
        numeroRastreador: ''
      });
    }, 2000);
    console.log('Form Values: ', this.formValues);
    this.formCadVehicle.valueChanges.pipe(
      debounceTime(1000))
      .subscribe(res => {
        console.log(res);
        this.formValues = res;
      });
  }

  cadastro() {
    console.log('Form Values: ', this.formValues);

    return this.httpClient.post('http://populus-alternative.herokuapp.com/cadonibus/?format=json', JSON.stringify(this.formValues), {
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8')
    })
      .subscribe((result => {
        this.dataPosted = result;
        console.log('Data Posted: ', this.dataPosted);
        this.router.navigate(['/dashboard']);
      }));
  }
}
