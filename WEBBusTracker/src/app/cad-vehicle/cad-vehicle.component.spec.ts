import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadVehicleComponent } from './cad-vehicle.component';

describe('CadVehicleComponent', () => {
  let component: CadVehicleComponent;
  let fixture: ComponentFixture<CadVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
