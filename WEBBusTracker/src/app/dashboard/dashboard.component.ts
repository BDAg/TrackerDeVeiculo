import { Component, OnInit, Self } from '@angular/core';
import { Vehicles } from '..//vehicle';
import { BusService } from '../providers/bus-service/bus-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  vehicles = Vehicles;

  bus: any;

  data: any = ['null'];

  constructor(public router: Router, public http: HttpClient, public busService: BusService) {

  }

  namePage = 'Dashboard';

  ngOnInit() {
    this.Atualizar();
  }

  Atualizar() {
    this.busService.callOnibus()
      .then(data => {
        this.data = data;
        console.log('Data: ', data);
      });
  }

  Deletar(id: number) {
    console.log('Deletando');
    this.http.delete(`https://populus-alternative.herokuapp.com/cadonibus/${id}/?format=json`, {
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8')
    })
      .subscribe(
        (val) => {
          console.log('DELETE Call Successfull', val);
        },
        response => {
          console.log('DELETE Call in error', response);
        },
        () => {
          console.log('OK');
        });
  }
}
