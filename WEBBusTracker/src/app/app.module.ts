import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CadUserComponent } from './cad-user/cad-user.component';
import { CadVehicleComponent } from './cad-vehicle/cad-vehicle.component';
import { MapaComponent } from './mapa/mapa.component';
import { AuthService } from './login/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { BusService } from './providers/bus-service/bus-service';
import { Config, DEV_CONFIG } from './providers/base-service';
import { ValidationService } from './validation.service';
import { ControlMessagesComponent } from './control-services.component';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { SearchPipe } from './search.pipe';
import { SortPipe } from './sort.pipe';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    CadUserComponent,
    CadVehicleComponent,
    MapaComponent,
    ControlMessagesComponent,
    SearchPipe,
    SortPipe
  ],
  imports: [
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    BusService,
    { provide : Config, useValue : DEV_CONFIG },
    ValidationService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
