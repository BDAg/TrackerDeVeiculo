import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    return items.filter( it => {
      let text = Object.keys(it)
        .map(key => {
          return it[key]
        })
        .join( " ");
      console.log('Text: ', text);
      return text.toLowerCase().includes(terms);
    })
  }
}
