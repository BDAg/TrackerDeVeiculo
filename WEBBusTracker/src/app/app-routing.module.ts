import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CadUserComponent } from './cad-user/cad-user.component';
import { CadVehicleComponent } from './cad-vehicle/cad-vehicle.component';
import { MapaComponent } from './mapa/mapa.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'cad-user', component: CadUserComponent, canActivate: [AuthGuard] },
  { path: 'cad-vehicle', component: CadVehicleComponent, canActivate: [AuthGuard] },
  { path: 'mapa/:rastreador', component: MapaComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
