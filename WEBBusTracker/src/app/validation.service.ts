import { FormGroup, ValidatorFn } from '@angular/forms';
declare var require:any;
var CPF = require("cpf_cnpj").CPF;


export class ValidationService {
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            'required': 'Esse campo é obrigatório',
            'invalidCreditCard': 'Is invalid credit card number',
            'invalidEmailAddress': 'Email inválido',
            'invalidPassword': 'Deve conter ao menos 6 caracteres   .',
            'minlength': `Mínimo de caracteres ${validatorValue.requiredLength}`,
            'invalidCpf':'CPF inválido',
            'equalValue':'Senhas diferentes',
        };

        return config[validatorName];
    }

    static creditCardValidator(control){
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
            return null;
        } else {
            return { 'invalidCreditCard': true };
        }
    }

    static emailValidator(control) {
        // RFC 2822 compliant regex
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else
        if(control.value == '' || control.value == null){
            return {'required':true};
        }   
        else 
            return { 'invalidEmailAddress': true };
        }
    

    static passwordValidator(control) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
            return null;
        } else {
            if(control.value == '' || control.value == null){
                return {'required':true};
            }else
            return { 'invalidPassword': true };
        }
    }
    static cpfValidator(control){
        if (control.value && CPF.isValid(control.value)) {
        return null;
        }
        else{
            if(control.value=="" || control.value==null){
                return{'required':true};
            }else
            return{'invalidCpf':true};
        }
    }
    
    static equalControlValue(targetKey: string, toMatchKey: string): ValidatorFn {

        return (group: FormGroup): { [key: string]: any } => {
    
            const target = group.controls[ targetKey ];
            const toMatch = group.controls[ toMatchKey ];
            if (target.touched && toMatch.touched) {
    
            const isMatch = target.value === toMatch.value;
    
            if (!isMatch && target.valid && toMatch.valid) {
                toMatch.setErrors({ equalValue: targetKey });
                return { 'equalValue': true };
            }
    
            if (isMatch && toMatch.hasError('equalControlValue')) {
                toMatch.setErrors(null);
            }
            }
    
            return null;
        };
    }
}