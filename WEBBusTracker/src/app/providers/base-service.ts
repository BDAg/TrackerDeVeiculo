import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Http } from "@angular/http";

export class Config {
    host: string;
    basePath: string;
    scheme: string;
    port: number;
}

export const DEV_CONFIG: Config = {
    host: "127.0.0.1",
    basePath: "/",
    scheme: "http",
    port: 8000
}

export class BaseService {

    constructor(
        public readonly config: Config,
        protected httpClient: Http
    ) {
    }

    public makeUrl(...path: Array<any>) {
        const fullPath = path.join('/') + '/';
        return `${this.config.scheme}://${this.config.host}:${this.config.port}${this.config.basePath}${fullPath}`;
    }
}


export class BaseModelService<E> extends BaseService {

    constructor(
        public readonly model: string,
        config: Config,
        httpClient: Http
    ) {
        super(config, httpClient);
    }

    cast( obj : any) : E{
        return obj as E;
    }

    castMany( obj : Array<any>) : Array<E> {
        return obj.map( it => this.cast(it));
    }

    get(id: number): Observable<E> {
        return this.httpClient.get(this.makeUrl(this.model, id)).pipe(
            map( it => it.json()),
            map( it => this.cast(it))
        );
    }

    list(filter: any = {}): Observable<Array<E>> {
        return this.httpClient.get(this.makeUrl(this.model), {
            params : filter
        }).pipe(
            map( it => it.json()),
            map( it => this.castMany(it as Array<any>))
        );
    }

    /*
    put(id: number, data: E): Observable<E> {

    }

    patch(id: number, data: any): Observable<E> {

    }

    delete(id: number): Observable<E> {

    }*/


}