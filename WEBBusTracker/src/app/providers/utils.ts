import { Position } from "./commons";


export const positionAsGoogle = ( position : Position ) : google.maps.LatLng => {
    return new google.maps.LatLng(position.lat,position.lon);
}