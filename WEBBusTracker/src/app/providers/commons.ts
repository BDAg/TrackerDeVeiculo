export interface Position {
    lat: number;
    lon: number;
}

export interface Bus {
    position: Position;
}
