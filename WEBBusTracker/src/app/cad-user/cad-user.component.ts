import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ValidationService } from '../validation.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-cad-user',
  templateUrl: './cad-user.component.html',
  styleUrls: ['./cad-user.component.css']
})
export class CadUserComponent implements OnInit {
  formCadUser;
  formValues: Object;

  dataPosted: any;


  constructor(private router: Router, private formBuilder: FormBuilder, public httpClient: HttpClient) { }

  ngOnInit() {
    this.formCadUser = this.formBuilder.group({
      nome: ['', Validators.minLength(2) ],
      cpf: ['', ValidationService.cpfValidator],
      tipoUser: ['',  Validators.required],
      email: ['',  ValidationService.emailValidator],
      senha: [''],
      senhaConfirm: [''],
    },
    { validator: ValidationService.equalControlValue('senha', 'senhaConfirm') }
    );
    setTimeout(() => {
      this.formCadUser.patchValue({
        nome: '',
        cpf: '',
        tipoUser: '',
        email: '',
        senha: '',
        senhaConfirm: '',
      });
    }, 2000);
    console.log(this.formValues);
    this.formCadUser.valueChanges.pipe(
      debounceTime(1000))
      .subscribe(res => {
        console.log(res);
        this.formValues = res;
      });
  }

  cadastro() {
    console.log('Form Values: ', this.formValues);

    return this.httpClient.post('http://populus-alternative.herokuapp.com/cadusuario/?format=json', JSON.stringify(this.formValues), {
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8')
    })
      .subscribe((result => {
        this.dataPosted = result;
        console.log('Data Posted: ', this.dataPosted);
        this.router.navigate(['/dashboard']);
      }));
  }
}
