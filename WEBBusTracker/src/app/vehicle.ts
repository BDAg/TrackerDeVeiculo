export class Vehicle {
    tipoVeiculo: string;
    categoria: string;
    marca: string;
    lat: any;
    lng: any;
    modelo: string;
    placa: string;
    frota: number;
    ativo: string;
    rastreador: number;
}

export const Vehicles: Vehicle[] = [
    {
        tipoVeiculo: 'Ônibus comum',
        categoria: 'Transporte publico',
        marca: 'Mercedez Benz',
        lat: -22.106952,
        lng:-50.165595,
        modelo: 'MB z-18',
        placa: 'FEO-2543',
        frota: 123,
        ativo: 'true',
        rastreador: 1
    },
    {
        tipoVeiculo: 'Ônibus acessível',
        categoria: 'Transporte publico',
        marca: 'Iveco',
        lat: -22.103042,
        lng: -50.183651,
        modelo: 'IV d-13',
        placa: 'NEO-4215',
        frota: 123,
        ativo: 'true',
        rastreador: 2
    },
    {
        tipoVeiculo: 'Ônibus comum',
        categoria: 'Transporte publico',
        marca: 'Mercedez Benz',
        lat: -22.074090,
        lng: -50.293153,
        modelo: 'MB o-15',
        placa: 'QZL-7849',
        frota: 124,
        ativo: 'false',
        rastreador: 3
    },
];
