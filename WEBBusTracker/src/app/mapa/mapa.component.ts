import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BusService } from './../providers/bus-service/bus-service';
import { positionAsGoogle } from './../providers/utils';
import { timeInterval } from 'rxjs/operators';
/// <reference types="@types/googlemaps" />

declare var google: any; 


@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})

export class MapaComponent implements OnInit {
  
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  
  constructor(
    private route: ActivatedRoute,
    public busService: BusService
  ) { }

  ngOnInit() {
    let id = 0
    
    id = this.getId()
    
    if (id != 0) {
      let onibus = this.busService.callTrackerById(id).then(onibus =>this.chamaOnibus(onibus.lat,onibus.lon))
      console.log(onibus)
    }
    else{
      console.log('genial')
    } 
    
  }
  
  getId(){
    const id = +this.route.snapshot.paramMap.get('rastreador');
    console.log(id)
    return id
  }

  chamaOnibus(lat,lon){
    console.log(lat,lon)
    let busao = new google.maps.LatLng(lat,lon)
    var map = {
      center: busao,
      streetViewControl: false,
      zoom: 16,
      mapTypeControlOptions: {      
        mapTypeIds: [
          google.maps.MapTypeId.ROADMAP,
          google.maps.MapTypeId.SATELLITE
        ]
      }

    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, map);
    
    let bus: google.maps.Marker = new google.maps.Marker({
      map: this.map,
      position: busao,
      icon: 'https://i.imgur.com/meMeD4H.png'
    })

    google.maps.event.addListener(bus,'click',(event)=>{
      infoBus.open(this.map,bus)
    });
    
    var infoBus = new google.maps.InfoWindow({
      content: "<h2>Linha: Marília x Tupã<h2> <br> <h3>Preço: R$ 7,10<h3> <h3>Guerino Seiscentos</h3> <h3>Modelo: Mercedez Benz</h3> <h4>Ônibus acessível</h4>" 
    });
  }
}